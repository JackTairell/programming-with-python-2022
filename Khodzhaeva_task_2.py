#!/usr/bin/env python
# coding: utf-8

# In[47]:


'''Write a function my_max:
Parameter: list of integers
Return value: largest number in the list
Do not use the built-in max() or min() functions !'''

def my_max(in_list):
    max_num = 'None'
    for i in in_list:
        if max_num == 'None':
            max_num = i
        elif i > max_num:
            max_num = i
    return(max_num)
print('print (my_max([-3, -6, -9, -16 ]))')
print(my_max([-3, -6, -9, -16 ]))
print()

'''Write a function process_list:
Parameter: list of integers
Return value: largest number in the list, after modification
Modification of the list:
If an elements is
odd, multiply the element by 2;
even, divide the element by 2.
If the position in the list is a multiple of 7, add the position to the value.'''

def process_list(in_list):
    for i in range(len(in_list)):
        if in_list[i] % 2 != 0:
            in_list[i] = in_list[i] * 2
        else:
            in_list[i] = in_list[i] / 2
        
        if i % 7 == 0:
            in_list[i] += i
        
        return my_max(in_list)
    
print('print(process_list([-3, -6, -9, -16 ]))')
print(process_list([-3, -6, -9, -16 ]))
print()


'''Write a function collatz
Input parameter: integer
Return value: integer
If the provided parameter is even, divide it by 2.
If the parameter is odd, multiply it by 3 and add one.
Repeat this step with the updated parameter, until a number occurs twice.
Return the value before the repeated value.'''

def collatz(integer):
    
    num_counter = [integer]
    
    while num_counter.count(integer) < 2:
        
        if integer % 2 == 0:
            integer = integer / 2
        else:
            integer = integer * 3 + 1
        num_counter.append(integer)
        
    return(num_counter[-2])

print('print(collatz(7))')
print(collatz(7))
print()

'''The actual Collatz Conjecture
Independently of the input, the sequence will reach value 1 at some point.'''


'''Write a function compute_pi:
Parameter: number of steps (Integer)
Return value: value of π (float)
Do not use math.pi, but the Leibniz formula:
(The sign alternates; the denominator takes every odd integer.)'''

def compute_pi(integer):
    pi_by_four = 1
    cur_dec = 1
    for i in range(integer):
        cur_dec +=2
        if i % 2 == 0:
            pi_by_four = pi_by_four - 1/cur_dec
        else: 
            pi_by_four = pi_by_four + 1/cur_dec
    return(pi_by_four * 4)

print('print(compute_pi(100000))')
print(compute_pi(100000))
print()

'''Write a function integer_cube_root(n)
Find the smallest k ∈ N, that satisfies for a given n ∈ N: k 3 ≥ n'''
def integer_cube_root(n):
    k = 1
    while k**3 < n:
        k +=1
    return k

print('print(integer_cube_root(3))')
print(integer_cube_root(3))
print()

'''Encryption
The Caesar encryption is an encryption method in which each letter is shifted by
three letters in the alphabet. Whitespace should be unchanged.
For a string with only upper case letters, compute the Caesar encryption.
For example, "HELLO WORLD" is encrypted as "KHOOR ZRUOG".
Hints:
i = ord(c) returns the ASCII code of character c.
c = chr(i) returns the character of ASCII code i.
Extend your function, such that a variable shift value can be specified.'''

def encryption(in_string, n):
    out_string = ''
    for letter in in_string:
        if letter == ' ':
            out_string = out_string + letter
            continue
            
        in_char_num = ord(letter)
        in_char_num += n
        out_char = chr(in_char_num)
        out_string = out_string + out_char
        
    return(out_string)

print("print(encryption('HELLO WORLD', 3))")
print(encryption('HELLO WORLD', 3))
print()

'''DNA Reverse Complement
Calculate the reverse complement of a DNA sequence (A ↔ T, C ↔ G).
Example: ACGATCGATCGATTC ↔ GAATCGATCGATCGT'''

def DNA_polymerase(DNA):
    code = {'A': 'T', 'G': 'C', 'C': 'G', 'T': 'A'}
    new_dna = []
    for nuc in DNA:
        new_dna.append(code[nuc])
    #print(new_dna)
    rev_dna = ''
    
    for i in range(len(new_dna)):
        nuc = new_dna.pop()
        rev_dna += nuc
        
    return (rev_dna)

print("print(DNA_polymerase('ACGATCGATCGATTC'))")
print(DNA_polymerase('ACGATCGATCGATTC'))
print()

'''Sorting
Sort a list using insertion sort (write your own code).
For an explanation, see https://en.wikipedia.org/wiki/Insertion_sort.'''

def silly_sort(in_list):
    i = 1
    while i < len(in_list):
        j = i
        while j > 0 and in_list[j-1] > in_list[j]:
            
            in_list[j], in_list[j-1] = in_list[j-1], in_list[j]
            j = j - 1
            
        i = i + 1
    return(in_list)

print('print(silly_sort([12, -4, 55, 108]))')
print(silly_sort([12, -4, 55, 108]))
print()

'''Calculate the first n Fibonacci numbers Fib[0:n], which are defined by
Fib[0] := 1 ,
Fib[1] := 1 ,
Fib[n] := Fib[n − 1] + Fib[n − 2] for n ≥ 2 .'''

def fibonacci(n):
    Fib = [1, 1]
    for i in range(2, n):
        new_fib = Fib[i - 1] + Fib[i - 2]
        Fib.append(new_fib)
        
    return(Fib)

print('print(fibonacci(6))')
print(fibonacci(6))
print()


# In[ ]:





# In[ ]:





# In[ ]:




