#!/usr/bin/env python
# coding: utf-8

# In[7]:


import math

#a
a = math.factorial(30) % 59
print(a)

#b
a = 2 ** 100 % 7
print(a)

#c
a = int('9' * 99) // 25
print(a)

#d
a = (33 ** 33).bit_length()
print(a)

#e
a = str(33 ** 33)
print(len(a))

#f
a = [-10, 5, 20, -35]
#print()

min_a = abs(a[0])
for x in a:
    if abs(x) < min_a:
        min_a = abs(x)
print(min_a)
    
max_a = abs(a[0])
for x in a:
    if abs(x) > max_a:
        max_a = abs(x)
print(max_a)

#print()
#g
r = 3
print(math.pi * (r ** 2))

#h
#a = math.comb(20, 10)
a = math.factorial(20) / (math.factorial(10) ** 2)
print(a)
#print('your_risk')

#i
n = 3100000000
print(math.log2(n))


# In[ ]:




