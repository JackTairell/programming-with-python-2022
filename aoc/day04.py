score = 0

with open('day04.txt') as t:
    for line in t:
        line_coords = line.strip().split(',')
        f_elf_range = line_coords[0].split('-')
        s_elf_range = line_coords[1].split('-')
        if (int(f_elf_range[0]) <= int(s_elf_range[0]) and int(f_elf_range[1]) >= int(s_elf_range[1])) or (
            int(f_elf_range[0]) >= int(s_elf_range[0]) and int(f_elf_range[1]) <= int(s_elf_range[1])):
            
            score += 1
            
print(score)
    

#part2
score = 0

with open('day04.txt') as t:
    for line in t:
        line_coords = line.strip().split(',')
        f_elf_range = line_coords[0].split('-')
        s_elf_range = line_coords[1].split('-')
        if (int(f_elf_range[0]) <= int(s_elf_range[0]) and int(f_elf_range[1]) >= int(s_elf_range[1])) or (
            int(f_elf_range[0]) >= int(s_elf_range[0]) and int(f_elf_range[1]) <= int(s_elf_range[1])) or (
            int(f_elf_range[0]) >= int(s_elf_range[0]) and int(f_elf_range[0]) <= int(s_elf_range[1])) or (
            int(f_elf_range[1]) >= int(s_elf_range[0]) and int(f_elf_range[1]) <= int(s_elf_range[1])):
            
            score += 1

print(score)
    