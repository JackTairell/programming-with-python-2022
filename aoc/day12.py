import string

def process_queue(start_pos, queue, coords2values, coords2neighbs):
    coords2values[start_pos] = 0
    

    while len(queue) > 0:
        cur_pos = 'begin:begin'
        for coord in queue:
            if cur_pos == 'begin:begin':
                cur_pos = coord
            elif coords2values[coord] < coords2values[cur_pos]:
                cur_pos = coord
                
        queue.pop(queue.index(cur_pos))
            
            
        for neighb in coords2neighbs[cur_pos]:
            
            nei_value = coords2values[cur_pos] + 1

            if coords2values[neighb] < nei_value:
                pass
            else:
                coords2values[neighb] = nei_value
            

map_lines = []
with open('day12.txt') as t:
    for line in t:
        map_lines.append(line.strip())

coords2neighbs = {}
alph = alphabet = string.ascii_lowercase

start_pos = ''
end_pos = ''
for line in range(len(map_lines)):
    for letter in range(len(map_lines[line])):
        if map_lines[line][letter] == 'S':

            start_pos = str(line) + ':' + str(letter)
            new_line = ''
            for letter2 in map_lines[line]:
                if letter2 != 'S':
                    new_line = new_line + letter2
                else:
                    new_line = new_line + 'a'
            map_lines[line] = new_line

        elif map_lines[line][letter] == 'E':
            end_pos = str(line) + ':' + str(letter)
            new_line = ''
            for letter2 in map_lines[line]:
                if letter2 != 'E':
                    new_line = new_line + letter2
                else:
                    new_line = new_line + 'z'
            map_lines[line] = new_line


for line in range(len(map_lines)):
    for letter in range(len(map_lines[line])):
        pos = str(line) + ':' + str(letter)
        if not pos in coords2neighbs:
            coords2neighbs[pos] = []

        item_score = alph.index(map_lines[line][letter])

        #looking up
        if line != 0:
            neighb = map_lines[line - 1][letter]
            nei_score = alph.index(neighb)
            nei_coords = str(line-1) + ':' + str(letter)
            if nei_score <= item_score + 1:
                coords2neighbs[pos].append(nei_coords)

        #looking_down
        if line != len(map_lines) - 1:
            neighb = map_lines[line + 1][letter]
            nei_score = alph.index(neighb)
            nei_coords = str(line + 1) + ':' + str(letter)
            if nei_score <= item_score + 1:
                coords2neighbs[pos].append(nei_coords)

        #looking_left
        if letter != 0:
            neighb = map_lines[line][letter - 1]
            nei_score = alph.index(neighb)
            nei_coords = str(line) + ':' + str(letter - 1)
            if nei_score <= item_score + 1:
                coords2neighbs[pos].append(nei_coords)

        #looking_right
        if letter != len(map_lines[0]) - 1:
            neighb = map_lines[line][letter + 1]
            nei_score = alph.index(neighb)
            nei_coords = str(line) + ':' + str(letter + 1)
            if nei_score <= item_score + 1:
                coords2neighbs[pos].append(nei_coords)

        
queue = []
for line in range(len(map_lines)):
    for letter in range(len(map_lines[line])):
        queue.append(str(line) + ':' + str(letter))

coords2values = {}
for coord in queue:
    coords2values[coord] = 100500 #(as the total number of fields in the map is less, this is equivalent to infinity)

process_queue(start_pos, queue, coords2values, coords2neighbs)

print('Part 1 answer:', coords2values[end_pos])



#part2
#cannot be run independently from part1, as it uses the modifications of all lists gained in the first part
#also, it is quite long. Here I print the lenght of a-position list and the numper of each iteration as an analogue to a progress bar


start_poss = []

for line in range(len(map_lines)):
    for letter in range(len(map_lines[line])):
        if map_lines[line][letter] == 'a':

            start_poss.append(str(line) + ':' + str(letter))

print('Progress bar: number of steps is ',len(start_poss))
pos_counter = 0
minimal_path = 100500
for start in start_poss:
    pos_counter += 1
    print('Progress bar: current step ',pos_counter)

    queue = []
    for line in range(len(map_lines)):
        for letter in range(len(map_lines[line])):
            queue.append(str(line) + ':' + str(letter))

    coords2values = {}
    for coord in queue:
        coords2values[coord] = 100500

    process_queue(start, queue, coords2values, coords2neighbs)
    if coords2values[end_pos] < minimal_path:
        minimal_path = coords2values[end_pos]

print('Part 2 answer:', minimal_path)