with open('day06.txt') as t:
    for line in t:
        line_coord = line.strip()
        for i in range(len(line_coord) - 4):
            pattern = line[i: i + 4]
            if len(list(set(pattern))) == 4:
                print(i + 4)
                break

#part2

with open('day06.txt') as t:
    for line in t:
        line_coord = line.strip()
        for i in range(len(line_coord) - 14):
            pattern = line[i: i + 14]
            if len(list(set(pattern))) == 14:
                print(i + 14)
                break