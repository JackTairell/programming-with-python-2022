score = 0

with open('day02.txt') as t:
    for line in t:
        line_coords = line.strip().split(' ')

        if (line_coords[0] == 'A' and line_coords[1] == 'Y') or (line_coords[0] == 'B' and line_coords[1] == 'Z') or (line_coords[0] == 'C' and line_coords[1] == 'X'):
            #win
            score += 6
        elif (line_coords[0] == 'A' and line_coords[1] == 'X') or (line_coords[0] == 'B' and line_coords[1] == 'Y') or (line_coords[0] == 'C' and line_coords[1] == 'Z'):
            #draw
            score += 3
        
        if line_coords[1] == 'X':
            score += 1
        elif line_coords[1] == 'Y':
            score += 2
        else:
            score += 3
        
print(score)


#part2
score = 0

with open('day02.txt') as t:
    for line in t:
        line_coords = line.strip().split(' ')
        
        if line_coords[1] == 'X':
            pass
        elif line_coords[1] == 'Y':
            score += 3
        else:
            score += 6
        
        if (line_coords[0] == 'A' and line_coords[1] == 'Y') or (line_coords[0] == 'B' and line_coords[1] == 'X') or (line_coords[0] == 'C' and line_coords[1] == 'Z'):
            #win
            score += 1
        elif (line_coords[0] == 'A' and line_coords[1] == 'Z') or (line_coords[0] == 'B' and line_coords[1] == 'Y') or (line_coords[0] == 'C' and line_coords[1] == 'X'):
            #win
            score += 2
        else: score += 3
        
print(score)