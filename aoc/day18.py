cube_coords = []

with open('day18.txt') as t:
    for line in t:
        line_coords = line.strip().split(',')
        cube_coords.append([int(x) for x in line_coords])

neigh_search_list = [[0, 0, -1], [0, -1, 0], [-1, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]]
pairs = {}
for cube in cube_coords:
    for modif in neigh_search_list:
        possible_coords = [0, 0, 0]
        for i in range(3):
            possible_coords[i] = cube[i] - modif[i]
        
        if possible_coords in cube_coords:
            pair = ','.join([str(x) for x in cube]) + ':' + ','.join(str(x) for x in possible_coords)
            rev_pair = ','.join(str(x) for x in possible_coords) + ':' + ','.join([str(x) for x in cube])
            if not pair in pairs or not rev_pair in pairs:
                pairs[pair] = '0'
                pairs[rev_pair] = '0'
            
score = len(cube_coords) * 6 - len(pairs)
print(score)


#No solution for part2