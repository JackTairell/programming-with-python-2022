def run_one_monk(monk, monkeys2stats):
    #print('monk = ', monk)
    items = monkeys2stats[monk]['items']
    oper = monkeys2stats[monk]['oper']
    test = monkeys2stats[monk]['test']
    monkeys2stats[monk]['item_counter']


    for item in items:
        #print(items)
        #print(item)
        if oper[0] == '+':
            item = item + oper[1]
        elif oper[0] == '*':
            item = item * oper[1]
        elif oper[0] == '^':
            item = item * item
        #print('item of question ', item)
        #item = int((item / 3) + 0.5)
        item = int((item / 3))
        monkeys2stats[monk]['item_counter'] += 1

        if item % test[0] == 0:
            monkeys2stats[test[1]]['items'].append(item)
            #print('true', item)
            #print(monkeys2stats[test[1]]['items'])
        else:
            monkeys2stats[test[2]]['items'].append(item)
            #print('false', item)
            #print(monkeys2stats[test[2]]['items'])
        #print(monkeys2stats[monk]['items'])
        #print('-------------------')
    monkeys2stats[monk]['items'] = []
    #print('______________')

    



def run_the_round(monk_list, monkeys2stats):
    for monk in monk_list:
        run_one_monk(monk, monkeys2stats)




lines = []
with open('day11.txt') as t:
    for line in t:
        lines.append(line.strip())

monkeys2stats = {}
monk_list = []

#here is the file parser
for i in range(0, len(lines), 7):
    monk_num = int(lines[i].split(' ')[1][:-1])
    if monk_num in monkeys2stats:
        print('Monkey ' + monk_num + ' already exists, check your input')
    monk_list.append(monk_num)
    monkeys2stats[monk_num] = {'items': [], 'oper': [], 'test' : [], 'item_counter': 0}
    
    it_line = lines[i + 1].split(':')[1].split(', ')
    for item in it_line:
        monkeys2stats[monk_num]['items'].append(int(item))

    oper_line = lines[i + 2].split(':')[1].split(' ')
    oper = oper_line[-2]
    const = oper_line[-1]
    if const == 'old':
        oper = '^'
        const = 2
    else: const = int(const)

    monkeys2stats[monk_num]['oper'] = [oper, const]

    test_check = int(lines[i + 3].split(' ')[-1])
    true_case = int(lines[i + 4].split(' ')[-1])
    false_case = int(lines[i + 5].split(' ')[-1])

    monkeys2stats[monk_num]['test'] = [test_check, true_case, false_case]


#print (monkeys2stats)

for i in range(20):
    #print('round = ', i)
    run_the_round(monk_list, monkeys2stats)

monk2activity = {}
for monk in monkeys2stats:
    monk2activity[monk] = monkeys2stats[monk]['item_counter']

sorted_monk = sorted(monk2activity.values())

print(sorted_monk[-2] * sorted_monk[-1])

#9215 too low
#56376 too low

#part 2

#here I create a new field in monkey data structure that for each item contains a list of 
#remainders of this item divided by every monkey's test value. After that I will operate with these lists instead of items.
#This will enable me to get rig of big numbers

monk2tests = []
for monk in monk_list:
    monk2tests.append(monkeys2stats[monk]['test'][0])

for monkey in monkeys2stats:
    monkeys2stats[monkey]['item_remainders'] = []
    for item in monkeys2stats[monkey]['items']:
        item_remainders = []
        for monk_test in monk2tests:
            remainder = item % monk_test
            item_remainders.append(remainder)
        monkeys2stats[monkey]['item_remainders'].append(item_remainders)



def run_one_monk2(monk, monkeys2stats, monk2tests):
    #print('monk = ', monk)
    items = monkeys2stats[monk]['item_remainders']
    oper = monkeys2stats[monk]['oper']
    test = monkeys2stats[monk]['test']
    monkeys2stats[monk]['item_counter']


    for item in items:
        #print(item)
        for rem_num in range(len(item)):
            #print(rem_num)
            #print(item[rem_num])
            if oper[0] == '+':
                item[rem_num] = (item[rem_num] + oper[1]) % monk2tests[rem_num] #since the remainder number of a monkey is the same as the number of this monkey in monk2tests
            elif oper[0] == '*':
                item[rem_num] = (item[rem_num] * oper[1]) % monk2tests[rem_num]
            elif oper[0] == '^':
                item[rem_num] = (item[rem_num] * item[rem_num]) % monk2tests[rem_num]
        
        monkeys2stats[monk]['item_counter'] += 1
        if item[monk] == 0:
            monkeys2stats[test[1]]['item_remainders'].append(item)
        else:
            monkeys2stats[test[2]]['item_remainders'].append(item)
    monkeys2stats[monk]['item_remainders'] = []

def run_the_round2(monk_list, monkeys2stats, monk2tests):
    for monk in monk_list:
        run_one_monk2(monk, monkeys2stats, monk2tests)

for i in range(10000):
    #print('round = ', i)
    run_the_round2(monk_list, monkeys2stats, monk2tests)

monk2activity = {}
for monk in monkeys2stats:
    monk2activity[monk] = monkeys2stats[monk]['item_counter']

sorted_monk = sorted(monk2activity.values())
print(sorted_monk[-2] * sorted_monk[-1])