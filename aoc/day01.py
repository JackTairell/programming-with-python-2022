max_elf = 0

cur_elf = 0
with open('day01.txt') as t:
    for line in t:
        if len(line.strip()) > 0:
            cur_elf += int(line.strip())
        else:
            if cur_elf > max_elf:
                max_elf = 0
                max_elf += cur_elf
            cur_elf = 0
print(max_elf)

#part2
elven_supplies = []

cur_elf = 0
with open('day01.txt') as t:
    for line in t:
        if len(line.strip()) > 0:
            cur_elf += int(line.strip())
        else:
            elven_supplies.append(cur_elf)
            cur_elf = 0

elven_supplies.sort(reverse=True)
print(elven_supplies[0] + elven_supplies[1] + elven_supplies[2])