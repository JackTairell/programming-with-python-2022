
score = 0
row_200_reachable = {}
sensor_coords = []

with open('day15.txt') as t:
    for line in t:
        line_coords = line.strip().split(': ')
        x_y_s = line_coords[0].split(', ')
        x_s = int(x_y_s[0].split('=')[1])
        y_s = int(x_y_s[1].split('=')[1])

        x_y_b = line_coords[1].split(', ')
        x_b = int(x_y_b[0].split('=')[1])
        y_b = int(x_y_b[1].split('=')[1])

        #print(x_s, y_s, x_b, y_b)
        manch_dist = abs(x_s - x_b) + abs(y_s - y_b)

        sensor_coords.append([x_s, y_s, manch_dist])
        
        way_dist = abs(2000000 - y_s)
        rest_dist = manch_dist - way_dist
        for x_coord in range(x_s - rest_dist, x_s +rest_dist +1, 1):
            if not (x_coord == x_b and y_b == 2000000):
                pos = str(x_coord) + ':2000000'
                row_200_reachable[pos] = '+'




#print(row_200_reachable)
print('Part 1 answer:',len(row_200_reachable))

#5181302 too high
#5181294 also too high
#3703989 wrong
#4709231 wrong
#7904661 wrong
#5100463 worked finally!!!

#part2

#It was written that in this region there is only one position that isn't covered by any sensor. Thus, it has to be right near to the edge of some radiuses.
#I try to create the list of neghbor positions to each radius and then check only this positions, whether they are reachable by other sensors. If not - that's the answer
#I create the dictionary of sensor coverage radiuses in part 1 not to run the file reader again.

def get_coords(coord):
    print('If you see this message, the conputer is not stuck yet')
    potential_beacons = []
    x_s = coord[0]
    md = coord[2] + 1
    y_s = coord[1] + md
    #going through the lines outside of sensor coverage radius, starting on the top and going clockwise
    start_x = coord[0]
    
    #line down-right:
    while x_s != start_x + md:
        if 0 <= x_s <= 4000000 and 0 <= y_s <= 4000000:
            potential_beacons.append([x_s, y_s])
        #print(potential_beacons[-1])
        x_s += 1
        y_s -= 1

    #then right-down
    while x_s != start_x:
        if 0 <= x_s <= 4000000 and 0 <= y_s <= 4000000:
            potential_beacons.append([x_s, y_s])
        #print(potential_beacons[-1])
        x_s -= 1
        y_s -= 1

    #down-left
    while x_s != start_x - md:
        if 0 <= x_s <= 4000000 and 0 <= y_s <= 4000000:
            potential_beacons.append([x_s, y_s])
        x_s -= 1
        y_s += 1
    #left-up
    while x_s != start_x:
        if 0 <= x_s <= 4000000 and 0 <= y_s <= 4000000:
            potential_beacons.append([x_s, y_s])
        x_s += 1
        y_s += 1

    return(potential_beacons)



final_coord = [0, 0]
for rad in sensor_coords:
    stop = 'no'
    potential_coords = get_coords(rad)
    for coord in potential_coords:
        x_pb = coord[0]
        y_pb = coord[1]
        not_in_rad = 0

        for rad2 in sensor_coords:
            x_s = rad2[0]
            y_s = rad2[1]
            md = rad2[2]

            md_pb = abs(x_pb - x_s) + abs(y_pb - y_s)

            if md_pb <= md:
                #potential_beacons.pop(potential_beacons.index(coord))
                not_in_rad = 1
                #print(coord, md_pb)
                #print(x_s, y_s, md)
                break
        
        final_coord = coord
        if not_in_rad == 0:
            stop = 'yes'
            print('Part 2 answer:', x_pb * 4000000 + y_pb)
            break
    if stop == 'yes':
        break
    
#print('not_working')
#print('if everything didn"t work', final_coord[0] * 4000000 + final_coord[1])
#print(potential_beacons)
