letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
let2num = {}
for i in range(len(letters)):
    let2num[letters[i]] = i + 1

priors = 0

with open('day03.txt') as t:
    for line in t:
        line_coord = line.strip()
        half = int(len(line_coord)/2)
        
        fh = line_coord[:half]
        sh = line_coord[half:]

        fh_set_list = list(set(fh))
        sh_set_list = list(set(sh))
        
        for let in fh_set_list:
            if let in sh_set_list:
                priors += let2num[let]
                break

print (priors)

#part2
letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
let2num = {}
for i in range(len(letters)):
    let2num[letters[i]] = i + 1

priors = 0
lines = []

with open('day03.txt') as t:
    for line in t:
        lines.append(line.strip())

for i in range(1, len(lines)-1, 3):
    f_line = lines[i - 1]
    s_line = lines[i]
    t_line = lines[i + 1]

    f_set_list = list(set(f_line))
    s_set_list = list(set(s_line))
    t_set_list = list(set(t_line))
    
    for let in f_set_list:
        if let in s_set_list and let in t_set_list:
            priors += let2num[let]
            break

print (priors)