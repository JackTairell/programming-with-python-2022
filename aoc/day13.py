def compare(item1, item2):
    #print(item1)
    #print(item2)
    if type(item1) == int and type(item2) == int:
        if item1 > item2:
            return (False)
        elif item1 < item2:
            return (True)
        else: return('Not_stated')

    elif type(item1) == int:
        item1 = [item1]

    elif type(item2) == int:
        item2 = [item2]
    
    if type(item1) == list and type(item2) == list:
        if len(item1) == 0 and len(item2) > 0:
            return(True)
        elif len(item1) > 0 and len(item2) == 0:
            return(False)
        elif len(item1) == 0 and len(item2) == 0:
            return ('Not_stated')

        for i in range(len(item1)):
            if len(item2) <= i:
                return (False)
            a = compare(item1[i], item2[i])
            if a == 'Not_stated':
                if len(item2) - i - 1 == 0 and i < len(item1) - 1:
                    return (False)
            else:
                return (a)
        return True


times2input = {}

with open('day13.txt') as t:
    counter = 0
    for line in t:
        if not counter in times2input:
            times2input[counter] = []
        if line.strip() != '':
            times2input[counter].append(eval(line.strip()))
        else:
            counter += 1

#print(times2input)

score = 0
false_cases = []
true_cases = []
for time in times2input:
    a = compare(times2input[time][0], times2input[time][1])
    #print(a)
    if a == True:
        true_cases.append(time)
        score += time + 1
    else:
        false_cases.append(time)

#print(true_cases)
print(score)



#part 2
import functools

def compare2(item1, item2):
    #print(item1)
    #print(item2)
    if type(item1) == int and type(item2) == int:
        if item1 > item2:
            return (-1)
        elif item1 < item2:
            return (1)
        else: return(0)

    elif type(item1) == int:
        item1 = [item1]

    elif type(item2) == int:
        item2 = [item2]
    
    if type(item1) == list and type(item2) == list:
        if len(item1) == 0 and len(item2) > 0:
            return(1)
        elif len(item1) > 0 and len(item2) == 0:
            return(-1)
        elif len(item1) == 0 and len(item2) == 0:
            return (0)

        for i in range(len(item1)):
            if len(item2) <= i:
                return (-1)
            a = compare2(item1[i], item2[i])
            if a == 0:
                if len(item2) - i - 1 == 0 and i < len(item1) - 1:
                    return (-1)
                elif len(item2) - i - 1 == 0 and i == len(item1)- 1:
                    return(0)
            else:
                return (a)
        return 1


input_times = []

with open('day13.txt') as t:
    counter = 0
    for line in t:
        if line.strip() != '':
            input_times.append(eval(line.strip()))

input_times += [[[2]], [[6]]]

#print(input_times)

sorted_times = sorted(input_times, key=functools.cmp_to_key(compare2), reverse=True)

pos2 = 0
for i in range(len(sorted_times)):
    if sorted_times[i] == [[2]]:
        pos2 = i + 1
        break
pos6 = 0
for i in range(len(sorted_times)):
    if sorted_times[i] == [[6]]:
        pos6 = i + 1
        break
#print(pos2)
#print(pos6)
#print(sorted_times)
print(pos2 * pos6)