states_in_cyces = []

with open('day10.txt') as t:
    x = 1
    for line in t:
        line_coords = line.strip().split(' ')
        if line_coords[0] == 'noop':
            states_in_cyces.append(x)
        elif line_coords[0] == 'addx':
            states_in_cyces.append(x)
            states_in_cyces.append(x)
            x += int(line_coords[1])
        else: print('smt went wrong, check the input file')

score = 0
for i in [20, 60, 100, 140, 180, 220]:
    score += states_in_cyces[i -1] * i
print(score)

#part2

states_in_cyces = []
rows = []
current_row = ''
letter_counter = 0


with open('day10.txt') as t:
    x = 1
    for line in t:
        # print('_____________')
        # print(line.strip())
        # print(letter_counter)
        # print(x)        

        line_coords = line.strip().split(' ')
        if line_coords[0] == 'noop':
            states_in_cyces.append(x)
            
            if letter_counter >= x - 1 and letter_counter <= x + 1:
                #print('noop, yes')
                current_row = current_row + '#'
                
            else:
                current_row = current_row + '.'
                #print('noop, no')
            
            letter_counter += 1
            if letter_counter == 40:
                rows.append(current_row)
                current_row = ''
                letter_counter = 0

        elif line_coords[0] == 'addx':
            states_in_cyces.append(x)
            if letter_counter >= x - 1 and letter_counter <= x + 1:
                current_row = current_row + '#'             
                #print('addx 1, yes')
            else:
                current_row = current_row + '.'
                #print('addx 1, no')

            letter_counter += 1
            if letter_counter == 40:
                rows.append(current_row)
                current_row = ''
                letter_counter = 0

            # print(letter_counter)
            # print(x)

            states_in_cyces.append(x)
            if (letter_counter >= x - 1) and (letter_counter <= x + 1):
                current_row = current_row + '#'
                #print('addx 2, yes')
            else:
                current_row = current_row + '.'
                #print('addx 2, no')
            
            letter_counter += 1 
            if letter_counter == 40:
                rows.append(current_row)
                current_row = ''
                letter_counter = 0
            
            x += int(line_coords[1])
            #print(x)
            
        else: print('smt went wrong, check the input file')

#print(rows)
for row in rows:
    print(row)

