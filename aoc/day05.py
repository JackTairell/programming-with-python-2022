
#here I create the column structure from the file with crate construction
stakes = {}
breaker = 0
line_counter = 0
with open('day05.txt') as t:
    for line in t:
        if line[1] == '1':
            breaker = 1
            break

        line_counter += 1
        col_counter = 0
        for i in range(len(line)):
            if (i + 1) % 2 == 0 and (i + 1) % 4 != 0:
                col_counter += 1
                if not col_counter in stakes:
                    stakes[col_counter] = []
                if line[i] != ' ':
                    stakes[col_counter].append(line[i])

for i in stakes:
    stakes[i].reverse()
#print(stakes)
#print(line_counter)
line_counter2 = 0
#here I go throudh the file with movement steps
with open('day05.txt') as t:
    for line in t:
        if line_counter2 <= line_counter + 1:
            line_counter2 += 1
            continue
        line_coords = line.strip().split(' ')
        number = int(line_coords[1])
        in_col = int(line_coords[3])
        out_col =int(line_coords[5])
        #print(number, in_col, stakes[in_col])

        for i in range(number):
            a = stakes[in_col].pop()
            stakes[out_col].append(a)

out_line = ''
for i in stakes:
    out_line = out_line + stakes[i][-1]

print(out_line)

#here I create the column structure from the file with crate construction
stakes = {}
breaker = 0
line_counter = 0
with open('day05.txt') as t:
    for line in t:
        #print(line)
        if line[1] == '1':
            breaker = 1
            break

        line_counter += 1
        col_counter = 0
        for i in range(len(line)):
            if (i + 1) % 2 == 0 and (i + 1) % 4 != 0:
                col_counter += 1
                if not col_counter in stakes:
                    stakes[col_counter] = []
                if line[i] != ' ':
                    stakes[col_counter].append(line[i])

for i in stakes:
    stakes[i].reverse()
#print(stakes)

#print(line_counter)

line_counter2 = 0
with open('day05.txt') as t:
    for line in t:
        if line_counter2 <= line_counter + 1:
            line_counter2 += 1
            continue
        line_coords = line.strip().split(' ')
        number = int(line_coords[1])
        in_col = int(line_coords[3])
        out_col =int(line_coords[5])
        #print(number, in_col, stakes[in_col])

        transfer_cases= []
        for i in range(number):
            a = stakes[in_col].pop()
            transfer_cases.append(a)
        transfer_cases.reverse()
        stakes[out_col] = stakes[out_col] + transfer_cases

out_line = ''
for i in stakes:
    out_line = out_line + stakes[i][-1]

print(out_line)

