t_coords = [0,0]
h_coords = [0,0]

tail_positions = ['0:0']




with open('day09.txt') as t:
    for line in t:
        line_coords = line.strip().split(' ')
        for i in range (int(line_coords[1])):
            #different cases for each direction
            
            if line_coords[0] == 'U':
                #we have to move tail somewhere, only if:

                #1) tail is now under the head - we move the tail up
                if t_coords[0] == h_coords[0] and h_coords[1] > t_coords[1]:
                    t_coords[1] += 1
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                #2) tail is diagonally under the head - we move tail on the position the head was on in the previous step
                elif (t_coords[0] < h_coords[0] and h_coords[1] > t_coords[1]) or (t_coords[0] > h_coords[0] and h_coords[1] > t_coords[1]):
                    t_coords = list(h_coords)
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                h_coords[1] += 1
            
            elif line_coords[0] == 'D':
                #we have to move tail somewhere, only if:

                #1) tail is now above the head - we move the tail down
                if t_coords[0] == h_coords[0] and h_coords[1] < t_coords[1]:
                    t_coords[1] -= 1
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                #2) tail is diagonally above the head - we move tail on the position the head was on in the previous step
                elif (t_coords[0] < h_coords[0] and h_coords[1] < t_coords[1]) or (t_coords[0] > h_coords[0] and h_coords[1] < t_coords[1]):
                    t_coords = list(h_coords)
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                h_coords[1] -= 1
            
            elif line_coords[0] == 'L':
                #we have to move tail somewhere, only if:

                #1) tail is now on the right of the head - we move the tail left
                if t_coords[1] == h_coords[1] and h_coords[0] < t_coords[0]:
                    t_coords[0] -= 1
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                #2) tail is diagonally on the right of the head - we move tail on the position the head was on in the previous step
                elif (t_coords[1] < h_coords[1] and h_coords[0] < t_coords[0]) or (t_coords[1] > h_coords[1] and h_coords[0] < t_coords[0]):
                    t_coords = list(h_coords)
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                h_coords[0] -= 1

            elif line_coords[0] == 'R':
                #we have to move tail somewhere, only if:

                #1) tail is now on the left of the head - we move the tail right
                if t_coords[1] == h_coords[1] and h_coords[0] > t_coords[0]:
                    t_coords[0] += 1
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                #2) tail is diagonally on the right of the head - we move tail on the position the head was on in the previous step
                elif (t_coords[1] < h_coords[1] and h_coords[0] > t_coords[0]) or (t_coords[1] > h_coords[1] and h_coords[0] > t_coords[0]):
                    t_coords = list(h_coords)
                    tail_positions.append(':'.join([str(x) for x in t_coords]))
                h_coords[0] += 1

pos_set = set(tail_positions)
#print(pos_set)
print(len(pos_set))

#part2

def process_knot(h_coords, t_coords):
    #here, the starting knot is already moved
    t_x = t_coords[0]
    t_y = t_coords[1]

    h_x = h_coords[0]
    h_y = h_coords[1]

    #we need to do something, only if:
    #1) H is on the same line (x or y) as T:
    #a) H is on the right - move tail to the right
    if t_y == h_y and h_x - t_x == 2:
        return [t_x + 1, t_y]
    #b)H is on the left - move tail to the left
    elif t_y == h_y and h_x - t_x == -2:
        return [t_x - 1, t_y]
    #c) H is right above - move tail up
    elif t_x == h_x and h_y - t_y == 2:
        return [t_x, t_y + 1]
    #c) H is right under - move tail down
    elif t_x == h_x and h_y - t_y == -2:
        return [t_x, t_y - 1]
        
    #2) H is direct diagonall to tail (the difference in both directions is 2)
    elif abs(t_x - h_x) == 2 and abs(t_y - h_y) == 2:
        new_coord = [int((h_x + t_x) / 2), int((h_y + t_y) / 2)]
        return new_coord
    
    #3) H is two points lower/above T and one point to the left/right
    elif abs(t_x - h_x) == 1 and abs(t_y - h_y) == 2:
        new_coord = [h_x, int((h_y + t_y) / 2)]
        return new_coord

    #4) H is one point lower/above T and two points to the left/right
    elif abs(t_x - h_x) == 2 and abs(t_y - h_y) == 1:
        new_coord = [int((h_x + t_x) / 2), h_y]
        return new_coord
    else:
        return t_coords


#part2

knot_order = ['h', 0, 1, 2, 3, 4, 5, 6, 7, 8]
knot_coords = {'h': [0,0], 0: [0,0], 1: [0,0], 2: [0,0], 3: [0,0], 4: [0,0], 5: [0,0], 6: [0,0], 7: [0,0], 8: [0,0]}
nineth_knot_poss = []

with open('day09.txt') as t:
    for line in t:
        operation = line.strip().split(' ')
        for i in range (int(operation[1])):
            #print('____________')
            #print(i)
            #print(knot_coords)
            #print('-------')
            if operation[0] == 'U':
                knot_coords['h'][1] += 1
            elif operation[0] == 'D':
                knot_coords['h'][1] -= 1
            elif operation[0] == 'L':
                knot_coords['h'][0] -= 1
            elif operation[0] == 'R':
                knot_coords['h'][0] += 1
            #print(knot_coords)

            for e in range(len(knot_order) - 1):
                h_coords = knot_coords[knot_order[e]]
                t_coords = knot_coords[knot_order[e + 1]]
                new_t_coords = process_knot(h_coords, t_coords)
                knot_coords[knot_order[e +1]] = list(new_t_coords)

                if knot_order[e+1] == 8:
                    #print(new_t_coords)
                    nineth_knot_poss.append(':'.join([str(x) for x in new_t_coords]))
            #print('------')
        #print(knot_coords)


pos_set = set(nineth_knot_poss)
#print(pos_set)
print(len(pos_set))





