rows = {}
columns = {}

with open('day08.txt') as t:
    line_counter = 0
    for line in t:
        if not line_counter in rows:
            rows[line_counter] = []
        line_coord = line.strip()
        for i in range(len(line_coord)):
            rows[line_counter].append(int(line_coord[i]))

            if not i in columns:
                columns[i] = []
            columns[i].append(int(line_coord[i]))

        line_counter += 1
#print (rows)
#print(len(columns[0]))
score = 0
coords = []
for row in range (len(rows)):
    if row == 0 or row == 98:
        score += len(rows[row])
        continue
    for i in range(len(columns)):
        if i == 0 or i == 98:
            score += 1
            continue
        coord = [row, i]
        tree_high = rows[row][i]

        if max(columns[i][:row]) < tree_high or max(columns[i][row + 1:]) < tree_high or max(rows[row][:i]) < tree_high or max(rows[row][i + 1:]) < tree_high:
            score += 1
print(score)
        

#part2

rows = {}
columns = {}

with open('day08.txt') as t:
    line_counter = 0
    for line in t:
        if not line_counter in rows:
            rows[line_counter] = []
        line_coord = line.strip()
        for i in range(len(line_coord)):
            rows[line_counter].append(int(line_coord[i]))

            if not i in columns:
                columns[i] = []
            columns[i].append(int(line_coord[i]))

        line_counter += 1
max_view = 0
coords = []
for row in range (len(rows)):
    for i in range(len(columns)):
        coord = [row, i]
        tree_high = rows[row][i]
        dims = [0, 0, 0, 0]

        #looking down
        if row + 1 < len(columns[0]):
            e = row + 1
            while e != len(columns[0]):
                if rows[e][i] < tree_high:
                    e += 1
                    dims[0] += 1
                else:
                    dims[0] += 1
                    break
        
        #looking up
        if row -1 >= 0:
            e = row - 1
            while e != -1:
                if rows[e][i] < tree_high:
                    e -= 1
                    dims[1] += 1
                else:
                    dims[1] += 1
                    break

        #looking right
        if i + 1 < len(rows[0]):
            e = i + 1
            while e != len(rows[0]):
                if rows[row][e] < tree_high:
                    e += 1
                    dims[2] += 1
                else:
                    dims[2] += 1
                    break
        
        #looking left
        if i -1 >= 0:
            e = i - 1
            while e != -1:
                if rows[row][e] < tree_high:
                    e -= 1
                    dims[3] += 1
                else:
                    dims[3] += 1
                    break
        
        
        dim_m = dims[0] * dims[1] * dims[2] * dims[3]
        if dim_m > max_view:
            max_view = dim_m

print(max_view)
        

        
