line_list = []

with open('day07.txt') as t:
    for line in t:
        line_list.append(line.strip())

current_path = '/'
current_folders = []
folder2size = {'/': 0}

for line_num in range(len(line_list)):
    line_coords = line_list[line_num].split(' ')
    if line_coords[0] == '$':
        #that's a command
        if line_coords[1] == 'cd':
            #different versions of where to change directories
            if line_coords[2] == '/':
                current_path = '/'
                current_folders = ['/']

            elif line_coords[2] == '..':
                #print('_____________')
                #print(current_folders)
                current_path = '/'.join(current_path.split('/')[0:-1])
                current_folders.pop()
                #print(current_folders)
                #print('__________________')
            
            else: 
                current_path = current_path + '/' + line_coords[2]
                current_folders.append(current_path)
                if not current_path in folder2size:
                    folder2size[current_path] = 0
        
        elif line_coords[1] == 'ls':
            i = line_num + 1
            while (line_list[i][0] != '$'):
                i += 1
                inner_line_coord = line_list[i-1].split(' ')
                #print(inner_line_coord)
                
                #different versions of what we can find there
                if inner_line_coord[0] == 'dir':
                    pass
                else:
                    size = int(inner_line_coord[0])
                    for folder in current_folders:
                        folder2size[folder] += size

                if i == len(line_list):
                    break

                

#print(folder2size)
score = 0
for folder in folder2size:
    if folder2size[folder] <= 100000:
        score += folder2size[folder]
print(score)

#part2

ovarall_space = 70000000
free_space = ovarall_space - folder2size['/']
space_to_free = 30000000 - free_space
#print(space_to_free)

smallest_bigger_dir = folder2size['/']
for folder in folder2size:
    if folder2size[folder] >= space_to_free:
        smallest_bigger_dir = min(smallest_bigger_dir, folder2size[folder])
        
print(smallest_bigger_dir)





