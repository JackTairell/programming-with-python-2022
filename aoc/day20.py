#The idea of this solution I got from the youtube video https://www.youtube.com/watch?v=gJuDsmBOgYQ&list=PL9MOh-GEjyTa8BpFQKIi4eDEn7yKcglxs&index=20
#The smart man there spend four times less time than me (before I gave up) to do this task, and the final idea he came up with is on timepoint 1:17:56

modified_list = []

line_counter = 0

with open('day20.txt') as t:
    for line in t:
        line_coord = line.strip()
        modified_list.append([int(line_coord), line_counter])
        line_counter += 1

#print(data)
lenn = len(modified_list)
inmodified_list = list(modified_list)

for item in inmodified_list:
    mov = item[0]
    mov_ind = modified_list.index((item))
    for i in range(abs(mov)):
        id_to_move = (mov_ind + (mov//abs(mov))) % lenn
        modified_list[mov_ind], modified_list[id_to_move] = modified_list[id_to_move], modified_list[mov_ind]
        mov_ind = id_to_move

    #print(data)



# part 1
for i in range(lenn):
    if modified_list[i][0] == 0:   
        break
print('Part 1 answer:', modified_list[(i + 1000) % lenn][0] + modified_list[(i + 2000) % lenn][0] + modified_list[(i + 3000) % lenn][0])


# part 2

iml_part2 = [(811589153 * mov, id_num) for mov, id_num in inmodified_list]
modified_list = list(iml_part2)

for time in range(10):
    print('Progress bar: ', time)
    for item in iml_part2:
        it_id = item[1]
        for i in range(lenn):
            if modified_list[i][1] == it_id:
                break
        
        mov_inds = modified_list[i][0] % (lenn - 1)
        for a in range(abs(mov_inds)):
            id_to_move = (i + (mov_inds//abs(mov_inds))) % lenn
            modified_list[i], modified_list[id_to_move] = modified_list[id_to_move], modified_list[i]
            i = id_to_move

for i in range(lenn):
    if modified_list[i][0] == 0:
        break

print('Part 2 answer:', modified_list[(i + 1000) % lenn][0] + modified_list[(i + 2000) % lenn][0] + modified_list[(i + 3000) % lenn][0])



#22865 too high
#4845 too low
#6640 worked