
def process_round(elves_positions, roulette_wheel):
    proposed_poss = {}
    for pos in elves_positions:
        coords = [int(x) for x in pos.split(':')]
        N = [coords[0] - 1, coords[1]]
        NE = [coords[0] - 1, coords[1] + 1]
        E = [coords[0], coords[1] + 1]
        ES = [coords[0] + 1, coords[1] + 1]
        S = [coords[0] + 1, coords[1]]
        SW = [coords[0] + 1, coords[1] - 1]
        W = [coords[0], coords[1] - 1]
        NW = [coords[0] - 1, coords[1] - 1]

        NWs = ':'.join([str(NW[0]), str(NW[1])])
        Ns = ':'.join([str(N[0]), str(N[1])])
        NEs = ':'.join([str(NE[0]), str(NE[1])])
        Es = ':'.join([str(E[0]), str(E[1])])
        ESs = ':'.join([str(ES[0]), str(ES[1])])
        Ss = ':'.join([str(S[0]), str(S[1])])
        SWs = ':'.join([str(SW[0]), str(SW[1])])
        Ws = ':'.join([str(W[0]), str(W[1])])
        
        #case all poss are clear
        if (not NWs in elves_positions and not Ns in elves_positions and not NEs in elves_positions and not Es in elves_positions and not ESs in elves_positions
        and not Ss in elves_positions and not SWs in elves_positions and not Ws in elves_positions):
            continue
        
        #grouping directions
        N_clear = False
        S_clear = False
        E_clear = False
        W_clear = False

        if not NWs in elves_positions and not Ns in elves_positions and not NEs in elves_positions:
            N_clear = True
        if not NEs in elves_positions and not Es in elves_positions and not ESs in elves_positions:
            E_clear = True
        if not ESs in elves_positions and not Ss in elves_positions and not SWs in elves_positions:
            S_clear = True
        if not SWs in elves_positions and not Ws in elves_positions and not NWs in elves_positions:
            W_clear = True
            # if pos == '6:0':
            #     print('it"s finally this')

        #case all directions not empty:
        if not (N_clear or E_clear or S_clear or W_clear):
            #print(pos)
            continue

        if roulette_wheel[0] == 'N':
            f_choice,  s_choice, t_choice, fr_choice = N_clear, S_clear, W_clear, E_clear
            f_dir, s_dir, t_dir, fr_dir = Ns, Ss, Ws, Es
        elif  roulette_wheel[0] == 'S':
            f_choice,  s_choice, t_choice, fr_choice = S_clear, W_clear, E_clear, N_clear
            f_dir, s_dir, t_dir, fr_dir = Ss, Ws, Es, Ns
        elif  roulette_wheel[0] == 'W':
            f_choice,  s_choice, t_choice, fr_choice = W_clear, E_clear, N_clear, S_clear
            f_dir, s_dir, t_dir, fr_dir = Ws, Es, Ns, Ss
        else:
            f_choice,  s_choice, t_choice, fr_choice = E_clear, N_clear, S_clear, W_clear
            f_dir, s_dir, t_dir, fr_dir = Es, Ns, Ss, Ws
        

        #case any direction is empty:
        if f_choice:
            if not f_dir in proposed_poss:
                proposed_poss[f_dir] = [pos]
            else: proposed_poss[f_dir].append(pos)
        
        elif s_choice:
            if not s_dir in proposed_poss:
                proposed_poss[s_dir] = [pos]
            else: proposed_poss[s_dir].append(pos)

        elif t_choice:
            if not t_dir in proposed_poss:
                proposed_poss[t_dir] = [pos]
            else: proposed_poss[t_dir].append(pos)
        
        elif fr_choice:
            if not fr_dir in proposed_poss:
                proposed_poss[fr_dir] = [pos]
            else: proposed_poss[fr_dir].append(pos)

        else: print('This cannot be, check your input', pos, N_clear, S_clear, W_clear, E_clear, Ws)

    #print(proposed_poss)
    #second part of the round
    for pos in proposed_poss:
        if len(proposed_poss[pos]) == 1:
            #print(pos)
            #print(proposed_poss[pos])
            del_pos = proposed_poss[pos][0]
            if pos in elves_positions:
                print('You are somehow moving to not empty direction, and this cannot be')
                continue
            elves_positions[pos] = '+'
            del elves_positions[del_pos]


def get_field_num(elves_positions):
    max_N = 100500
    max_S = -100500
    max_W = 100500
    max_E = -100500

    pos_counter = 0
    for pos in elves_positions:
        coords = [int(x) for x in pos.split(':')]
        max_N = min(max_N, coords[0])
        max_S = max(max_S, coords[0])
        max_W = min(max_W, coords[1])
        max_E = max(max_E, coords[1])

    #print(max_N, max_E, max_W, max_E)
    for line_num in range(max_N, max_E + 1):
        for let_num in range(max_W, max_E + 1):
            pos = ':'.join([str(line_num), str(let_num)])
            if not pos in elves_positions:
                pos_counter += 1

    return (pos_counter)




lines = []
with open('day23.txt') as t:
    for line in t:
        lines.append(line.strip())
        
elves_positions = {}
for line_num in range(len(lines)):
    for let_num in range(len(lines[line_num])):
        if lines[line_num][let_num] == '#':
            pos = ':'.join([str(line_num), str(let_num)])
            elves_positions[pos] = '+'



#print(elves_positions)
#print(len(elves_positions))
roulette_wheel = ['N', 'S', 'W', 'E']
for i in range(10):
    #print(i)
    process_round(elves_positions, roulette_wheel)
    a = roulette_wheel.pop(0)
    roulette_wheel.append(a)

print('Part 1 answer:', get_field_num(elves_positions))

#2657 - too low
#4034 - worked!!!

#part2
lines = []
with open('day23.txt') as t:
    for line in t:
        lines.append(line.strip())
        
elves_positions = {}
for line_num in range(len(lines)):
    for let_num in range(len(lines[line_num])):
        if lines[line_num][let_num] == '#':
            pos = ':'.join([str(line_num), str(let_num)])
            elves_positions[pos] = '+'



still_going = True
roulette_wheel = ['N', 'S', 'W', 'E']
round_counter = 0

while still_going:
    prev_dict_state = dict(elves_positions)
    round_counter += 1
    #print(round_counter)
    process_round(elves_positions, roulette_wheel)
    a = roulette_wheel.pop(0)
    roulette_wheel.append(a)

    # print(prev_dict_state)
    # print('______________________')
    # print(elves_positions)


    if elves_positions == prev_dict_state:
        still_going = False
        print('Part 2 answer: ', round_counter)

#print(get_field_num(elves_positions))
