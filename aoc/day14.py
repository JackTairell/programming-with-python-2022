def process_vector(start_pos, end_pos, rock_poss):
    #four possible cases: rock expanding up, down, right or left
    #exp up:
    if start_pos[0] == end_pos[0] and start_pos[1] < end_pos[1]:
        for i in range(start_pos[1], end_pos[1] + 1):
            pos = str(start_pos[0]) + ':' + str(i)
            rock_poss[pos] = 'cave'

    #exp down:
    elif start_pos[0] == end_pos[0] and start_pos[1] > end_pos[1]:
        for i in range(start_pos[1], end_pos[1] - 1, -1):
            pos = str(start_pos[0]) + ':' + str(i)
            rock_poss[pos] = 'cave'

    #exp right:
    elif start_pos[1] == end_pos[1] and start_pos[0] < end_pos[0]:
        for i in range(start_pos[0], end_pos[0] + 1):
            pos = str(i) + ':' + str(start_pos[1])
            rock_poss[pos] = 'cave'

    #exp left:
    elif start_pos[1] == end_pos[1] and start_pos[0] > end_pos[0]:
        for i in range(start_pos[0], end_pos[0] - 1, -1):
            pos = str(i) + ':' + str(start_pos[1])
            rock_poss[pos] = 'cave'



def process_sand(rock_poss):
    cur_sand_pos = [500, 0]
    sand_piece_falling = True
    void = rock_poss['void']

    while sand_piece_falling:
        if cur_sand_pos[1] == void:
            sand_piece_falling = False
            return False


        down_pos = ':'.join([str(cur_sand_pos[0]), str(cur_sand_pos[1] + 1)])
        down_left_pos = ':'.join([str(cur_sand_pos[0] - 1), str(cur_sand_pos[1] + 1)])
        down_right_pos = ':'.join([str(cur_sand_pos[0] + 1), str(cur_sand_pos[1] + 1)])

        if not down_pos in rock_poss:
            cur_sand_pos = [cur_sand_pos[0], cur_sand_pos[1] + 1]
            #print(cur_sand_pos)

        elif not down_left_pos in rock_poss:
            cur_sand_pos = [cur_sand_pos[0] - 1, cur_sand_pos[1] + 1]
            #print(cur_sand_pos)
        
        elif not down_right_pos in rock_poss:
            cur_sand_pos = [cur_sand_pos[0] + 1, cur_sand_pos[1] + 1]
            #print(cur_sand_pos)

        else:
            sand_piece_falling = False

            if cur_sand_pos == [500, 0]:
                return False
            elif down_pos in rock_poss or down_left_pos in rock_poss or down_right_pos in rock_poss:
                pos = ':'.join([str(cur_sand_pos[0]), str(cur_sand_pos[1])])
                rock_poss[pos] = 'sand'
                #print(cur_sand_pos)
                #print('__________________________')
                return True
            else: print ("This can't be, check your input")









rock_poss = {}
max_y = 0

with open('day14.txt') as t:
    for line in t:
        line_coords = line.strip().split(' -> ')
        for pos_num in range(1, len(line_coords)):
            start_pos = [int(x) for x in line_coords[pos_num - 1].split(',')]
            end_pos = [int(x) for x in line_coords[pos_num].split(',')]
            process_vector(start_pos, end_pos, rock_poss)


            if start_pos[1] > max_y:
                max_y = start_pos[1]
            elif end_pos[1] > max_y:
                max_y = end_pos[1]

            rock_poss['void'] = max_y



sand_falling = True
#sand_fall_counter = 0
while sand_falling:
    #sand_fall_counter += 1
    #print(sand_fall_counter)
    sand_falling = process_sand(rock_poss)

sand_counter = 0
for pos in rock_poss:
    if rock_poss[pos] == 'sand':
        sand_counter += 1

#print(rock_poss)

print(sand_counter)


#part2
def process_sand_floor(rock_poss):
    cur_sand_pos = [500, 0]
    sand_piece_falling = True
    void = rock_poss['void']

    while sand_piece_falling:
        if cur_sand_pos[1] + 1 == void:
            sand_piece_falling = False
            pos = ':'.join([str(cur_sand_pos[0]), str(cur_sand_pos[1])])
            rock_poss[pos] = 'sand'
            return True


        down_pos = ':'.join([str(cur_sand_pos[0]), str(cur_sand_pos[1] + 1)])
        down_left_pos = ':'.join([str(cur_sand_pos[0] - 1), str(cur_sand_pos[1] + 1)])
        down_right_pos = ':'.join([str(cur_sand_pos[0] + 1), str(cur_sand_pos[1] + 1)])

        if not down_pos in rock_poss:
            cur_sand_pos = [cur_sand_pos[0], cur_sand_pos[1] + 1]
            #print(cur_sand_pos)

        elif not down_left_pos in rock_poss:
            cur_sand_pos = [cur_sand_pos[0] - 1, cur_sand_pos[1] + 1]
            #print(cur_sand_pos)
        
        elif not down_right_pos in rock_poss:
            cur_sand_pos = [cur_sand_pos[0] + 1, cur_sand_pos[1] + 1]
            #print(cur_sand_pos)

        else:
            sand_piece_falling = False

            if cur_sand_pos == [500, 0]:
                return False
            elif down_pos in rock_poss or down_left_pos in rock_poss or down_right_pos in rock_poss:
                pos = ':'.join([str(cur_sand_pos[0]), str(cur_sand_pos[1])])
                rock_poss[pos] = 'sand'
                #print(cur_sand_pos)
                #print('__________________________')
                return True
            else: print ("This can't be, check your input")


floor = max_y + 2
rock_poss['void'] = floor

sand_falling = True
#sand_fall_counter = 0
while sand_falling:
    #sand_fall_counter += 1
    #print(sand_fall_counter)
    sand_falling = process_sand_floor(rock_poss)

sand_counter = 0
for pos in rock_poss:
    if rock_poss[pos] == 'sand':
        sand_counter += 1

#print(rock_poss)

print(sand_counter + 1)